package ru.t1.nikitushkina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
public class User extends AbstractModel {

    private static final long serialVersionUId = 1;

    @Nullable
    @Column(nullable = false)
    private String login;

    @Nullable
    @Column(name = "password", nullable = false)
    private String passwordHash;

    @Nullable
    @Column(length = 100)
    private String email;

    @Nullable
    @Column(name = "first_name", length = 100)
    private String firstName;

    @Nullable
    @Column(name = "last_name", length = 100)
    private String lastName;

    @Nullable
    @Column(name = "middle_name", length = 100)
    private String middleName;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Column
    @Nullable
    private Date created = new Date();

    @Column
    @NotNull
    private Boolean locked = false;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Session> sessions = new ArrayList<>();

}
