package ru.t1.nikitushkina.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.nikitushkina.tm.api.repository.dto.IDTORepository;
import ru.t1.nikitushkina.tm.comparator.CreatedComparator;
import ru.t1.nikitushkina.tm.comparator.StatusComparator;
import ru.t1.nikitushkina.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractDTORepository<M extends AbstractModelDTO> implements IDTORepository<M> {

    @Getter
    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @Override
    public void clear() throws Exception {
        @NotNull final String jpql = "DELETE FROM " + getClazz().getSimpleName() + " m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getClazz()).getResultList();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) throws Exception {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getClazz());
        @NotNull final Root<M> from = criteriaQuery.from(getClazz());
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws Exception {
        return entityManager.find(getClazz(), id);
    }

    @NotNull
    protected abstract Class<M> getClazz();

    @Override
    public int getSize() throws Exception {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) throws Exception {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

    @Override
    public void remove(@NotNull final M model) throws Exception {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final M model) throws Exception {
        entityManager.merge(model);
    }

}
