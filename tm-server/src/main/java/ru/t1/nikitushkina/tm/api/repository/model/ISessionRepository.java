package ru.t1.nikitushkina.tm.api.repository.model;

import ru.t1.nikitushkina.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
