package ru.t1.nikitushkina.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;
import ru.t1.nikitushkina.tm.dto.request.UserRegistryRequest;
import ru.t1.nikitushkina.tm.dto.response.UserRegistryResponse;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.event.ConsoleEvent;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

@Component
public final class UserRegistryListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-registry";

    @NotNull
    public static final String DESCRIPTION = "Registry user.";

    @Override
    @EventListener(condition = "@userRegistryListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER REGISTRY]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.print("ENTER EMAIL: ");
        @NotNull final String email = TerminalUtil.nextLine();
        System.out.print("ENTER LAST NAME: ");
        @NotNull final String lstName = TerminalUtil.nextLine();
        System.out.print("ENTER FIRST NAME: ");
        @NotNull final String fstName = TerminalUtil.nextLine();
        System.out.print("ENTER MIDDLE NAME: ");
        @NotNull final String mdlName = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken());
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        request.setLstName(lstName);
        request.setFstName(fstName);
        request.setMdlName(mdlName);
        @Nullable UserRegistryResponse response = userEndpointClient.registryUser(request);
        @NotNull final UserDTO user = response.getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
