package ru.t1.nikitushkina.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public class ProjectUpdateByIndexResponse extends AbstractProjectResponse {

    public ProjectUpdateByIndexResponse(@Nullable ProjectDTO project) {
        super(project);
    }

}
