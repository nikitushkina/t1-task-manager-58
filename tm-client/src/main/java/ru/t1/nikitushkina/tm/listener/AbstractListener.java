package ru.t1.nikitushkina.tm.listener;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.nikitushkina.tm.api.IListener;
import ru.t1.nikitushkina.tm.api.service.ITokenService;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.event.ConsoleEvent;

@Getter
@Setter
@Component
@NoArgsConstructor
public abstract class AbstractListener implements IListener {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    public abstract void execute(@NotNull final ConsoleEvent consoleEvent);

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract String getName();

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @NotNull
    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += ": " + description + "\n";
        return result;
    }

}
