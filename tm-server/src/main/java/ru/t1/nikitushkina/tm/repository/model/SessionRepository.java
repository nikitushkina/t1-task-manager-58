package ru.t1.nikitushkina.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.nikitushkina.tm.api.repository.model.ISessionRepository;
import ru.t1.nikitushkina.tm.model.Session;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class SessionRepository extends AbstractUserOwnedRepository<Session>
        implements ISessionRepository {

    @NotNull
    @Override
    protected Class<Session> getClazz() {
        return Session.class;
    }

}
