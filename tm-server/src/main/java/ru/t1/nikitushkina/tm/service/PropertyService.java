package ru.t1.nikitushkina.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";
    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";
    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";
    @NotNull
    public static final String GIT_BRANCH = "gitBranch";
    @NotNull
    public static final String GIT_COMMIT_ID = "gitCommitId";
    @NotNull
    public static final String GIT_COMMIT_TIME = "gitCommitTime";
    @NotNull
    public static final String GIT_COMMIT_MSG_FULL = "gitCommitMsgFull";
    @NotNull
    public static final String GIT_COMMITTER_NAME = "gitCommitterName";
    @NotNull
    public static final String GIT_COMMITTER_EMAIL = "gitCommitterEmail";

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;
    @Value("#{environment['password.secret']}")
    private String passwordSecret;
    @Value("#{environment['server.port']}")
    private String serverPort;
    @Value("#{environment['server.host']}")
    private String serverHost;
    @Value("#{environment['session.key']}")
    private String sessionKey;
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;
    @Value("#{environment['database.username']}")
    private String dBUser;
    @Value("#{environment['database.password']}")
    private String dBPassword;
    @Value("#{environment['database.url']}")
    private String dBUrl;
    @Value("#{environment['database.driver']}")
    private String dBDriver;
    @Value("#{environment['database.dialect']}")
    private String dBDialect;
    @Value("#{environment['database.show_sql']}")
    private String dBShowSql;
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String dBHbm2ddlAuto;
    @Value("#{environment['database.format_sql']}")
    private String formatSQL;
    @Value("#{environment['database.second_lvl_cache']}")
    private String dBSecondLvlCache;
    @Value("#{environment['database.factory_class']}")
    private String dBFactoryClass;
    @Value("#{environment['database.use_query_cache']}")
    private String dBUseQueryCache;
    @Value("#{environment['database.use_min_puts']}")
    private String dBUseMinPuts;
    @Value("#{environment['database.region_prefix']}")
    private String dBRegionPrefix;
    @Value("#{environment['database.config_file_path']}")
    private String dBConfigFilePath;
    @Value("#{environment['database.default_schema']}")
    private String dBSchema;
    @Value("#{environment['database.init_token']}")
    private String tokenInit;
    @Value("#{environment['database.liquibase_config']}")
    private String liquibaseConfig;
    @Value("#{environment['jms.url']}")
    private String jmsUrl;
    @Value("#{environment['empty.value']}")
    private String emptyValue;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @NotNull
    @Override
    public String getCommitMsgFull() {
        return read(GIT_COMMIT_MSG_FULL);
    }

    @NotNull
    @Override
    public String getCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @NotNull
    @Override
    public String getCommitterEmail() {
        return read(GIT_COMMITTER_EMAIL);
    }

    @NotNull
    @Override
    public String getCommitterName() {
        return read(GIT_COMMITTER_NAME);
    }

    @NotNull
    @Override
    public String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return emptyValue;
        if (!Manifests.exists(key)) return emptyValue;
        return Manifests.read(key);
    }

}
